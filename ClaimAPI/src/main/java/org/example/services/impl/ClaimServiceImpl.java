package org.example.services.impl;

import org.example.dtos.response.ClaimDto;
import org.example.entity.Claim;
import org.example.entity.Proof;
import org.example.entity.Status;
import org.example.repository.ClaimRepository;
import org.example.repository.ProofRepository;
import org.example.services.ClaimService;
import org.example.tools.DtoTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClaimServiceImpl implements ClaimService {

    @Autowired
    private ClaimRepository _claimRepository;
    @Autowired
    ProofRepository _proofRepository;

    @Autowired
    private DtoTools _dtoTools;


    @Override
    public ClaimDto createClaim(ClaimDto claimDto) throws Exception {
        Claim claim = _dtoTools.convertToEntity(new Claim(), claimDto);
        int nullId = 0;
        List<Proof> proofs = new ArrayList<>();
        Proof proof = new Proof("https://vl-media.fr/wp-content/uploads/2022/03/Picsou-75-ans-anniversaire-Disney-Carl-Barks-La-Bande-a-Picsou-Picsou-Magazine-1024x694.jpg");
        proofs.add(proof);
        claim.setProofs(proofs);
        proof.setClaim(claim);
        if (claim.getDate() != null && claim.getCategory() != null && claim.getCompany() != null && claim.getPrice() != 0 && !claim.getProofs().isEmpty() ) {
            Claim claim1 = _claimRepository.save(claim);
            return _dtoTools.convertToDto(claim1, new ClaimDto());
        } else {
            throw new Exception("Remplir les champs svp");
        }
    }

    @Override
    public ClaimDto readClaim(int id) {
        return _dtoTools.convertToDto(_claimRepository.findById(id).get(), new ClaimDto());
    }

    @Override
    public List<ClaimDto> readClaims() {
        List<ClaimDto> claimDtos = new ArrayList<>();
        List<Claim> claims = (List<Claim>) _claimRepository.findAll();
        for (Claim claim : claims) {
            claimDtos.add(_dtoTools.convertToDto(claim, new ClaimDto()));
        }
        return claimDtos;
    }

    @Override
    public List<ClaimDto> getClaimByIdUser(int idUser) {
        List<ClaimDto> claimDtos = new ArrayList<>();
        List<Claim> claims = (List<Claim>) _claimRepository.findClaimsByIdUser(idUser);
        for (Claim claim : claims) {
            claimDtos.add(_dtoTools.convertToDto(claim, new ClaimDto()));
        }
        return claimDtos;
    }

    @Override
    public ClaimDto updateStatusClaim(int id) {
        Claim claim = _claimRepository.findById(id).get();

        if (claim.getStatus() == Status.PENDING){
            claim.setStatus(Status.VALIDATED);
            _claimRepository.save(claim);
        }else if (claim.getStatus() == Status.VALIDATED){
            claim.setStatus(Status.REFUND);
            _claimRepository.save(claim);
        }

        return _dtoTools.convertToDto(claim, new ClaimDto());
    }

    @Override
    public ClaimDto updateClaim(int id, String date, String category, String company, double price) {
        Claim claim = _claimRepository.findById(id).get();

        claim.setDate(date);
        claim.setCategory(category);
        claim.setCompany(company);
        claim.setPrice(price);
        _claimRepository.save(claim);

        return _dtoTools.convertToDto(claim, new ClaimDto());
    }
}
