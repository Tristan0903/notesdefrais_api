package org.example.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.entity.Proof;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClaimDto {

    private int id;
    private int idUser;
    private String date;
    private String category;
    private String company;
    private double price;
    private List<Proof> proofs;
}
