package org.example.repository;

import org.example.entity.Claim;
import org.springframework.data.repository.CrudRepository;

public interface ProofRepository extends CrudRepository<Claim, Integer> {
}
