// const express = require("express")
// const cors = require("cors")
// const { post } = require("./api.service")
import express from "express"
import cors from "cors"
import {createClaim, getAllClaims, getClaimById, login, register, updateStatus} from "./services/api.service.js"


const app = express()

app.use(cors({ origin: "*", methods: ["POST", "GET", "PATCH"] }))

app.use(express.json())

//USER API

app.post("/user/register", async (req, res) => {
    const response =  await register("/user/register",{...req.body})
    return res.json(response.data);
})

app.post("/user/login", async (req, res) => {
    const response = await login("/user/login", {...req.body})
    return res.json(response.data)
})

//CLAIM API
app.post("/claim" , async (req, res) => {
    const response = await createClaim("/claim", {...req.body})
    return res.json(response.data)
})

app.get("/claim", async (req, res) => {
    const response = await getAllClaims("/claim")
    return res.json(response.data)
})

app.get("/claim/:id", async (req, res)=> {
    const id = req.params.id
    const response = await getClaimById("/claim/status/"+id, id)
    return res.json(response.data)
})

app.patch("/claim/status/:id", async (req, res) => {
    const id = req.params.id
    const response = await updateStatus("/claim/status/"+id, id)
    return res.json(response.data)
})

app.listen(8084, () => {
    console.log("Server is running!")
})