import axios from "axios";


const roots = {
    "user": "http://localhost:8080/user",
    "claim": "http://localhost:8081/api/claim",
}

//USERAPI SERVICE
export const register = async (api, data) => {
    return await axios.post(roots.user+"/register", data);
}

export const login = async (api, data) => {
    return await axios.post(roots.user+"/login", data);
}


//CLAIMAPI SERVICE
export const createClaim = async (api, data) => {
    return await axios.post(roots.claim+"", data)
}

export const getAllClaims = async (api) => {
    return await axios.get(roots.claim+"")
}

export const getClaimById = async (api, id) => {
    return await axios.get(roots.claim+"/"+id)
}

export const updateStatus = async (api, id) => {
    return await axios.patch(roots.claim+"/status/"+id)
}